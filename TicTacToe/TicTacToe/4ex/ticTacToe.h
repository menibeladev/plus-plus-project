/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#include <string>

#ifndef TICTACTOE_H
#define TICTACTOE_H

using namespace std;

class Player
{
private:
	string* name; // string that holds the name of the player
	char symbol; // Player's playing symbol 'X' or 'O'
	int* currentPlayingIndex; // An array to hold index like this: {1,2} means x: 1 and y: 2
public:
	Player(); // Constructor
	~Player(); // Deconstructor

	bool setPlayingIndex(); // Gets its indexes from console
	int * getPlayingIndex(); // Returns a pointer to array of size two

	void setName(string name); // Name setter
	string getName(); // Name getter

	void setSymbol(char x); // Symbol setter
	char getSymbol(); // Symbol getter
};

class Game
{
private:

	char board[3][3]; // The 3x3 board of tictactoe
	void cleanBoard(); // Iterate through the board and change every cell in it to ' ' (space value)
	Player* playersPlaying[2]; // Array of 2 players playing
	int currentPlayerIndex; // Index of the player that is currently playing
	bool isThereAWinner(); // Iterate through the board to check if there's a winner
	bool isThereADraw(); // Checks if there's a draw 
	bool initializePlayersFromConsole(); // Initialize the players (called when startGame() is called)

	bool play(int x, int y); // Place a new move on the board
	bool ready; // Defines if the game is currently running or has stopped
	void printCurrentState(); // Print the board

public:
	Game(); // Constructor
	~Game(); // Deconstructor

	void startGame(); // Starts the game
};

#endif