/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#include <iostream>
#include <string>
#include "ticTacToe.h"

using namespace std;

// Definitions of Player class:
Player::Player()
{
	this->currentPlayingIndex = new int[2]; // Initialize the new array dynamically
	this->name = new string; // Initialize the string variable dynamically
}


Player::~Player()
{
	delete currentPlayingIndex; // Delete dynamically allocated memory for the index array
	delete name; // Delete dynamically allocated memory for string
}

string Player::getName()
{
	return *this->name; // Return this object's name
}

void Player::setName(string name)
{
	*this->name = name; // Receives a string and set it to this object's name string value
}

char Player::getSymbol() 
{
	return this->symbol; // Returns the char symbol of this player
}

void Player::setSymbol(char c) 
{
	if (c != 'O' && c != 'X'){ // Check if C is a valid symbol

	}
	else // c Is a valid symbol
	{ 
		this->symbol = c;
	}
}

bool Player::setPlayingIndex()
{
	bool result = false; // Assume false, change it accordingly

	int x, y;  // Indexes
	char coma; // Placeholder for coma between two indexes
	cin >> x >> coma >> y; // Get user input in the form of "1,2"

	if (x > 0 && x <= 3 && y > 0 && y <= 3 && coma == ',') 
	{
		// If both inputs are valid, and coma between them was detected
		currentPlayingIndex[0] = x;
		currentPlayingIndex[1] = y;
		result = true;
	} // If input is not valid, 'result' will be false
	return result;
}

int * Player::getPlayingIndex() 
{
	return this->currentPlayingIndex; // Returns the currently playing indexes that the player chose in setPlayingIndex() function
}


// Definitions for Game
Game::Game()
{

}

Game::~Game()
{
	delete playersPlaying[0]; // Delete dynamically allocated memory for object of Player class allocated at initializePlayersFromConsole()
	delete playersPlaying[1]; // Delete dynamically allocated memory for object of Player class allocated at initializePlayersFromConsole()
}

void Game::printCurrentState() // Iterate through the board array and print every value in it in the correct format
{
	for (int x = 0; x < 3; x++)
	{
		cout << "|";
		for (int y = 0; y < 3; y++) 
		{
			cout << "  " << board[x][y] << "  |";
		}

		cout << "\n";
		x < 2 ? cout << "-------------------" << endl : cout << ""; // Dont print that line at the bottom of the board
	}
}

void Game::cleanBoard()
{
	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 3; y++) {
			this->board[x][y] = ' '; // Change every value inside the board to a space
		}
	}
}

bool Game::play(int x, int y) 
{
	bool result = false; // Assume play has failed to 


	if (this->ready) // Check if play() was not accidentaly called without starting the game (shouldn't happen)
	{ 
		x--; // Decrease by one so it will match the regular indexing of array
		y--; // Decrease by one so it will match the regular indexing of array

		if (x > 0 || x <= 3 || y > 0 || y <= 3) { // valid index inserted
			if (board[x][y] == ' '){ // if spot is empty

				this->board[x][y] = playersPlaying[currentPlayerIndex]->getSymbol(); // Take current player's symbol and place it in the x,y location
				result = true; // Function succeeded
			}
		}
	}
	if (result)// If the play was successful, switch the players
	{ 
		// Switch turns of players
		currentPlayerIndex = (currentPlayerIndex == 1 ? 0 : 1);

		// Check to see if to continue the game or stop it
		if (this->isThereAWinner()) // Winner detected
		{
			cout << "Winner: " << this->playersPlaying[!currentPlayerIndex]->getName() << endl; // Print winner's name
			ready = false; // Stop the game
		}
		else if (this->isThereADraw()) { // Check if there's a draw
			cout << "No winners" << endl; // Print message to console
			ready = false; // Stop the game
		}
	}
	return result; // If placement of X or O in the spot was successful, this will be true
}

bool Game::isThereADraw() // Checks if every spot in the board is taken already, if it is - return true
{
	bool result = true; // Assume all spaces of the array are already taken, i.e not empty
	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 3; y++) {
			// Iterate through every cell in 'board'
			if (this->board[x][y] == ' ') {
				// An empty cell found, statement is false;
				result = false;
				break; // Stop searching for more cells
			}
		}
		if (!result){
			break; // Stop searching for more cells, result is false already
		}
	}
	return result;
}

void Game::startGame(){
	this->cleanBoard(); // Clean the board (from junk values)

	currentPlayerIndex = 0; // Set the first player of the array to play first

	if (initializePlayersFromConsole()){
		// Initialized players succcessfully

		this->ready = true; // Game is ready

		while (this->ready) { // For as long as game is not stopped by a draw or a winner

			// Print the board 
			this->printCurrentState();


			if (this->playersPlaying[currentPlayerIndex]->setPlayingIndex()) {
				// If index to play was successfully received from the console


				// Take current players's playing indexes, and call the play() function with those values
				this->play(playersPlaying[currentPlayerIndex]->getPlayingIndex()[0], playersPlaying[currentPlayerIndex]->getPlayingIndex()[1]);


			}
		}
		// End of while loop, game is over
	}
}


bool Game::initializePlayersFromConsole() {
	bool success = false; // Assume function has failed, change it accordingly

	// Clear the cin's buffer
	cin.clear();
	cin.ignore(INT_MAX, '\n');

	string* playerOnesName = new string; // Initialize a new string dynamically

	if (playerOnesName) { // If dynamic memory allocation was successful
		getline(cin, *playerOnesName); // Get first name from user

		playersPlaying[0] = new Player; // Initialize a new Player object (first Player of the array) dynamically (Those will be deleted on Game's deconstructor)

		if (playersPlaying[0] != NULL){ // If dynamic memory allocation was successful

			playersPlaying[0]->setName(*playerOnesName); // Set its name
			playersPlaying[0]->setSymbol('X'); // Set its symbol, first player : X

			// Clear the cin's buffer
			cin.clear();

			getline(cin, *playerOnesName);  // Get second name from user 

			playersPlaying[1] = new Player; // Initialize the second player dynamically (Those will be deleted on Game's deconstructor)
			if (playersPlaying[1] != NULL){ // If successful
				playersPlaying[1]->setName(*playerOnesName); // Set player2's name
				playersPlaying[1]->setSymbol('O'); // Set its symbol : O

				success = true; // If reached here, function was successful
			}
		}
	}
	delete playerOnesName; // Release memory of the string used to assign the player's names
	return success; // If all went well, this should be true
}

bool Game::isThereAWinner() {

	if (board[0][0] == board[0][1] && board[0][1] == board[0][2] && (board[0][0] == 'X' || board[0][0] == 'O')){ // First horizontal line
		return true;
	}
	else if (board[1][0] == board[1][1] && board[1][1] == board[1][2] && (board[1][0] == 'X' || board[1][0] == 'O')){ // Second horizontal line
		return true;
	}
	else if (board[2][0] == board[2][1] && board[2][1] == board[2][2] && (board[2][0] == 'X' || board[2][0] == 'O')){ // Third horizontal line
		return true;
	}
	else if (board[0][0] == board[1][0] && board[1][0] == board[2][0] && (board[0][0] == 'X' || board[0][0] == 'O')){ // First vertical line
		return true;
	}
	else if (board[0][1] == board[1][1] && board[1][1] == board[2][1] && (board[0][1] == 'X' || board[0][1] == 'O')){ // Second vertical line
		return true;
	}
	else if (board[0][2] == board[1][2] && board[1][2] == board[2][2] && (board[0][2] == 'X' || board[0][2] == 'O')){ // Third vertical line
		return true;
	}
	else if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && (board[0][0] == 'X' || board[0][0] == 'O')){ // Right Diagonal line
		return true;
	}
	else if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && (board[2][0] == 'X' || board[2][0] == 'O')) { // Left Diagonal line
		return true;
	}
	else { // No winner
		return false;
	}
}
