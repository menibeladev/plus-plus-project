/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#include <iostream>
#include "TicTacToe.h"
#include <string>

using namespace std;

int main()
{
	cout << "Exe4 Meni Beladev 307836684 and Paz Goldmacher 313167546" << endl;
	cout << "Hello ' please enter your choice : " << endl;
	cout << "2 Play Tic-Tac-Toe" << endl;
	cout << "3 Exit" << endl;
	while (true) 
	{
		int userSelection = 0; // Initialize a new int of user's choise
		if (cin >> userSelection){ // Check if userSelection is a number
			if (userSelection == 3) {
				break; // If userSelection is 3: break the 'while loop', thus exiting program
			}
			else if (userSelection == 2) {
				// 2 Selected, play ticTacToe

				// Start playing 
				Game* newGame = new Game;

				newGame->startGame(); // While loop inside this function will keep running as long as the game is active

				//startGame() ended, delete newGame to release memory
				delete newGame;
			}
			else { // Invalid number input (not 2, not 3)

				// Clear the cin's buffer
				cin.clear();
				cin.ignore(INT_MAX, '\n');
			}
		}
		else { // Input was not a number	
			// Clear the cin's buffer
			cin.clear();
			cin.ignore(INT_MAX, '\n');
		}
	}

	return 0; // End of main()
}