/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/
#include "WarPlayer.h"
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

WarPlayer::WarPlayer(char* name) : name(name)
{
	// Reset arrays values to zero
	this->liveCards = NULL;
	this->deadCards = NULL;
	this->liveCardsAmount = 0;
	this->deadCardsAmount = 0;
	this->liveCards = new Card*[1];
	this->deadCards = new Card*[1];
}
WarPlayer::WarPlayer(WarPlayer &warPlayerCopy) : name(warPlayerCopy.getName())
{
	//Copy its live cards
	this->liveCardsAmount = warPlayerCopy.liveCardsAmount;
	this->liveCards = new Card*[this->liveCardsAmount];
	for (int liveCard = 0; liveCard < this->liveCardsAmount; liveCard++)
	{
		this->liveCards[liveCard] = new Card(*warPlayerCopy.liveCards[liveCard]);
	}
	//Copy its dead cards
	this->deadCardsAmount = warPlayerCopy.deadCardsAmount;
	this->deadCards = new Card *[this->deadCardsAmount];
	for (int deadCard = 0; deadCard < this->deadCardsAmount; deadCard++) 
	{
		this->deadCards[deadCard] = new Card(*warPlayerCopy.deadCards[deadCard]);
	}
}

WarPlayer::~WarPlayer() //Destructor
{
	cout << this->getName() << " deleted." << endl;
}

const char* WarPlayer::getName()
{
	return this->name; //Returns the name of the WarPlayer
}

int WarPlayer::getLiveCardAmount() 
{
	return this->liveCardsAmount; //Returns size of liveCards array
}

int WarPlayer::getDeadCardAmount()
{
	return this->deadCardsAmount; //Return size of deadCards array
}

void WarPlayer::addLiveCard(Card &card) 
{
	//Add a card to the liveCards array
	addCardToList(card, this->liveCards, this->liveCardsAmount);
}

void WarPlayer::addDeadCard(Card &card)
{
	//Add a card to the liveCards array
	addCardToList(card, this->deadCards, this->deadCardsAmount);
}

void WarPlayer::addCardToList(Card &card, Card** &cardsList, int &amount)
{
	if (amount == 0) //cardsList is currently empty (size == 0)
	{ 
		//This is a new card to an empty list
		cardsList = new Card*[1]; //Set its size to 1
		cardsList[0] = new Card(card); //Go to first index and put a new card there
		amount = 1; //Change size to 1
	}
	else //List is not empty
	{ 
		Card** copyHolder = new Card*[amount + 1]; //Copy holder array @ size of current + 1

		//Copy all liveCards content to it
		for (int i = 0; i < amount; i++)
		{
			copyHolder[i] = new Card(*cardsList[i]);
		}

		//Go to last index of copyHolder and add the last card (Card &card)
		copyHolder[amount] = new Card(card);

		//Re-declare liveCards with new values (its size increased by 1)
		cardsList = new Card*[amount + 1];
		amount++; //Amount increased by one

		for (int j = 0; j < amount; j++) //Copy content from copyHolder array to liveCards
		{
			cardsList[j] = new Card(*copyHolder[j]);
		}

		delete[] copyHolder; //Delete copyHolder and release its memory
	}
}

void WarPlayer::removeCardFromList(const int &index, Card** &cardsList1, int &amount)
{
	delete cardsList1[index]; //Release dynamically allocated memory for current card
	cardsList1[index] = NULL; //Set its value in memory to NULL

	//Iterate through all the Cards ahead in the list, and move them 1 spot backwards
	//Thus reducing the size of the array by 1
	for (int x = index; x < amount - 1; x++) 
	{
		cardsList1[x] = new Card(*cardsList1[x + 1]);
	}
	amount--; //Update the size of array by reducing it by one
}

void WarPlayer::removeLiveCard(const int &index)
{
	//Call removeCardFromList (private function) with received index from Live Cards
	this->removeCardFromList(index, this->liveCards, this->liveCardsAmount);
}

void WarPlayer::removeDeadCard(const int &index) 
{
	//Call removeCardfromList (private function) with received index from Dead Crds
	this->removeCardFromList(index, this->deadCards, this->deadCardsAmount);
}

Card* WarPlayer::pickCard()
{
	if (this->liveCardsAmount == 0 && this->deadCardsAmount > 0) 
	{
		//If there are not any live cards, migrate all deadCards to liveCards
		//Migrate everything from deadCards to LiveCards and pick a random card from liveCards

		for (int x = 0; x < this->deadCardsAmount; x++) 
		{
			//For every dead card in deadCardsList:

			//Add it to the live cards
			this->addCardToList(*this->deadCards[x], this->liveCards, this->liveCardsAmount);

			//Remove it from the deadCards list
			this->removeCardFromList(x, this->deadCards, this->deadCardsAmount);
		}
	}
	else if (this->liveCardsAmount == 0 && this->deadCardsAmount == 0) 
	{
		//If function is called from an object of WarPlayer that has no cards, display an error (shouldn't happen)
		cout << "Cannot pick a card because player " << this->getName() << " has no cards!" << endl;
		return NULL;
	}
	//liveCards amount should not be zero here 
	//Pick a random number from 0 to liveCardsAmount
	srand(time(NULL));
	int randomNumber = rand() % this->liveCardsAmount;

	//Create a new card from liveCards list at index: randomNumber
	Card* pickedCard = new Card(*this->liveCards[randomNumber]);

	//Remove the card from the list (it has been picked)
	removeLiveCard(randomNumber);

	//Return the card
	return pickedCard;
}

int WarPlayer::getCardsValue() 
{
	int totalCardsValue = 0;

	//Sum up all the values of liveCards
	for (int live = 0; live < this->liveCardsAmount; live++)
	{
		totalCardsValue = totalCardsValue + this->liveCards[live]->getNumber();
	}

	//Sum up all the values of deadCards
	for (int dead = 0; dead < this->deadCardsAmount; dead++) 
	{
		totalCardsValue = totalCardsValue + this->deadCards[dead]->getNumber();
	}

	return totalCardsValue;
}

int WarPlayer::getTotalNumberOfCards()
{
	//Add the total amount of cards WarPlayer has
	return this->liveCardsAmount + this->deadCardsAmount;
}

bool WarPlayer::hasCards()
{
	//if both liveCardsAmount and DeadCards amount are 0, no cards remain
	return this->liveCardsAmount > 0 || this->deadCardsAmount > 0;
}

void WarPlayer::print()
{
	//Print the WarPlayer's data with accordance to the exercise description
	cout << "Player: " << this->getName() << "\n";
	cout << "Number of live Cards: " << this->liveCardsAmount << ", deadCards: " << this->deadCardsAmount << ", their total value: " << this->getCardsValue() << endl;
}