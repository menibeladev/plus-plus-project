/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#ifndef WARPLAYER_H
#define WARPLAYER_H
#include "Deck.h"
#include "Card.h"

class WarPlayer
{
private:
	const char* name; //Holds the name of the player
	Card** liveCards; //Array of pointers to cards (Live Cards)
	Card** deadCards; //Array of pointers to cards (Dead Cards)
	int liveCardsAmount; //Size of liveCards
	int deadCardsAmount; //Size of deadCards
	void addCardToList(Card &card, Card** &cardsList, int &amount); // Add a card to a certain cardslist
	void removeCardFromList(const int &index, Card** &cardsList, int &amount); // Remove a card from a certain cardslist

public:
	WarPlayer(char* name); //Constructor to recieve a name
	WarPlayer(WarPlayer &warPlayerCopy); //Copy constructor
	~WarPlayer(); //Destructor
	const char* getName(); //Get the name of the player
	int getLiveCardAmount(); //Get the size of LiveCards array
	int getDeadCardAmount(); //Get the size of DeadCards array
	void addLiveCard(Card &card); //Public function to add live card
	void addDeadCard(Card &card); //Public function to add dead card
	void removeLiveCard(const int &index); // Public function to remove a deadCard
	void removeDeadCard(const int &index); // Public function to remove a liveCard
	Card* pickCard();
	/* Randomally pick a card from liveCards,
	if no cards are there - transfer all from dead to live and pick one card,
	if both are empty: returns NULL*/
	int getCardsValue(); //Get WarPlayer's total cards value
	int getTotalNumberOfCards(); //Get the total number of cardss the WarPlayer has
	bool hasCards(); //True if player has more than 0 cards in either dead or live cards
	void print(); //Prints WarPlayer's data to console

};

#endif

