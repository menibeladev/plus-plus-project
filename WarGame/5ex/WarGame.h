/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#ifndef WARGAME_H
#define WARGAME_H
#include "Deck.h"
#include "Card.h"
#include "WarPlayer.h"

class WarGame
{
public:
	WarPlayer *player1; //A pointer player1
	WarPlayer *player2; //A pointer player2
	Deck *deck;
	WarGame(char* playerOnesName, char* playerTwosname); //A constructor who accepts the names of the players
	WarGame(WarGame &warGameCopy); //Copy constructor
	~WarGame(); //Destructor
	void printStatus(); //Print the intermediate result
	WarPlayer* startGame(int rounds); //A function that begins the game
	bool addCardToList(Card &card, Card** &cardsList, int &amount);

};

#endif