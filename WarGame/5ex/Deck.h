/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#ifndef DECK_H
#define DECK_H
#include "Card.h"
	

class Deck
{
private:
	bool deleteCardFromDeck(int index); //Removing a card from the card stack
	int sizeOfDeck; //Pack size cards

public:
	Deck(); //Default constructor
	Deck(Deck &copyDeck); //Copy onstructor
	~Deck(); // Destructor
	Card pickCard(); //Retrieving a random card
	Card pickCard(int cardIndex); //Extracting a card at a certain location
	void printDeck(); //Print all cards in the deck
	Card *cards[52]; //Array of Card objects

};

#endif