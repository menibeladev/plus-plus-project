/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/
#include "Card.h"
#include "Deck.h"
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

Deck::Deck()
{
	sizeOfDeck = 0; //Initialize size of deck = 0
	for (int typeCounter = 1; typeCounter <= 4; typeCounter++)
	{
		for (int numberCounter = 1; numberCounter <= 13; numberCounter++)
		{
			//Iterate through 4 types and 13 different numbers to generate all possible cards out of the 52
			cards[sizeOfDeck] = new Card(typeCounter, numberCounter);
			sizeOfDeck++;
		}
	}
}

Deck::Deck(Deck &copyDeck) 
{
		this->sizeOfDeck = 0; //Initialize size of deck = 0
		for (int cardCounter = 0; cardCounter < copyDeck.sizeOfDeck; cardCounter++)
		{
			// Iterate through the copyDeck's cards
			if (copyDeck.cards[cardCounter] != NULL) //If it's not null, add it (double check)
			{ 
				this->cards[sizeOfDeck] = new Card(*copyDeck.cards[cardCounter]);
				this->sizeOfDeck++;
			}
		}
	}

Deck::~Deck()
{
	//Delete every card pointer inside the deck
	for (int x = 0; x < this->sizeOfDeck; x++) {
		delete cards[x];
	}

	cout << "Deck was deleted" << endl;
}

Card Deck::pickCard()
{
	//Generate a random number between zero and the size of deck
	srand(time(NULL));
	int randomNumber = rand() % (this->sizeOfDeck);
	//Call pickCard with the randomly generated value
	return pickCard(randomNumber);
}
Card Deck::pickCard(int cardIndex)
{
	// Pick a card from card's array at index: cardIndex
	Card discardedCard(*this->cards[cardIndex]);

	// Remove it from the deck
	deleteCardFromDeck(cardIndex);

	// Return the card
	return discardedCard;
}

bool Deck::deleteCardFromDeck(int index)
{
	bool success = false; //Assume failure

	delete this->cards[index]; //Free the dynamically allocated memory for this card
	this->cards[index] = NULL; //Remove its data

	for (int x = index; x < this->sizeOfDeck - 1; x++)
	{
		//For every card in the current array of cards, take the next card in the index and move it 1 spot backwards 
		this->cards[x] = new Card(*this->cards[x + 1]);

		delete this->cards[x + 1];
	}

	success = true;

	//Decrease size of deck by one
	this->sizeOfDeck = this->sizeOfDeck - 1;
	return success;
}
void Deck::printDeck() //Print all cards in the deck
{
	for (int x = 0; x < this->sizeOfDeck; x++) 
	{
		cout << "card index: " << x;
		cards[x]->printCard();
	}
}
