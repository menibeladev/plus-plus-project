/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#ifndef CARD_H
#define CARD_H

class Card
{
private:
	const int type; //Fixed card type 
	const int number; //Fixed card number

public:
	Card(); //Default Constructor
	Card(int type, int number); //Constructor Parameters
	Card(Card &copyCard); //Copy constructor
	~Card(); //Destructor
	int getNumber(); // Number getter
	int getType(); // Type getter
	void printCard(); //Print card data

};

#endif