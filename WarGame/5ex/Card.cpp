/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/
#include "Card.h"
#include <iostream>

using namespace std;
Card::Card() : type(1), number(1) //Empty Default constructor
{
}
Card::Card(int type1, int number1) : type(type1), number(number1) //Constructor Parameters
{
}
Card::Card(Card &copyCard) : type(copyCard.getType()), number(copyCard.getNumber())//Copy constructor
{
}
Card::~Card() //Destructor
{
}
int Card::getNumber() //Return card's number value
{
	return this->number;
}
int Card::getType() //Return card's type value
{
	return this->type;
}
void Card::printCard() //Print card
{
	cout << "Card: " << getNumber() << ", type:" << getType() << endl;
}
