/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/
#include "WarGame.h"
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

WarGame::WarGame(char* playerOnesName, char* playerTwosname)
{
	//Create a new WarPlayer with names received as parameters
	this->player1 = new WarPlayer(playerOnesName);
	this->player2 = new WarPlayer(playerTwosname);

	this->deck = new Deck(); //Create a new deck (Randomally drawed 52 cards)

	for (int i = 0; i < 52; i++)
	{
		if (i % 2 == 0)
		{
			//Every even index card goes to player1
			this->player1->addLiveCard(deck->pickCard());
		}
		else
		{
			//Every odd index card goes to player2
			this->player2->addLiveCard(deck->pickCard());
		}
	}

	//Print data to console
	player1->print();
	player2->print();
	cout << endl;
}

WarGame::WarGame(WarGame &warGameCopy) //Copy data from warGameCopy
{
	//Copy player1
	this->player1 = new WarPlayer(*warGameCopy.player1);
	//Copy player2
	this->player2 = new WarPlayer(*warGameCopy.player2);
	//Copy the deck
	this->deck = new Deck(*warGameCopy.deck);
}

WarGame::~WarGame()
{
	cout << "Leading player is: " <<
		(player1->getCardsValue() > player2->getCardsValue() ? player1->getName() : player2->getName()) << "\n";
	cout << endl;

	//Release dynamically allocated memory allocated in the constructor
	delete player1;
	delete player2;
	delete deck;
}

void WarGame::printStatus()
{
	cout << player1->getName() << " has: " << player1->getLiveCardAmount() <<
		" live cards and: " << player1->getDeadCardAmount() << " dead cards. " <<
		" total cards: " << (player1->getDeadCardAmount() + player1->getLiveCardAmount()) << "\n"
		" total value of cards: " << player1->getCardsValue() << endl;

	cout << player2->getName() << " has: " << player2->getLiveCardAmount() <<
		" live cards and: " << player2->getDeadCardAmount() << " dead cards. " <<
		" total cards: " << (player2->getDeadCardAmount() + player2->getLiveCardAmount()) << "\n"
		" total value of cards: " << player2->getCardsValue() << endl;
}

WarPlayer* WarGame::startGame(int rounds)
{
	for (int x = 1; x <= rounds; x++)  //Iterate through every round for the number of rounds received as parameter
	{
		cout << "---------Round number: " << x << "---------" << endl;

		if (player1->hasCards() && player2->hasCards()) //If both players has cards
		{
			//Each player picks 1 card
			Card* player1Card = new Card(*this->player1->pickCard());
			Card* player2Card = new Card(*this->player2->pickCard());

			//Print them to the console 
			cout << player1->getName() << "'s card: " << player1Card->getNumber() << ", ";
			cout << player2->getName() << "'s card: " << player2Card->getNumber() << "\n";

			//Compare the two cards
			if (player1Card->getNumber() > player2Card->getNumber()) //If the player1 drew a higher card
			{
				//Player 1 won this round, add both cards to player 1's dead list

				player1->addDeadCard(*player1Card);
				player1->addDeadCard(*player2Card);

				cout << player1->getName() << " wins the round" << endl;
			}
			else if (player1Card->getNumber() < player2Card->getNumber()) //If the player2 drew a higher card
			{
				//Player 2 won this round, add both cards to player 1's dead list

				player2->addDeadCard(*player1Card);
				player2->addDeadCard(*player2Card);

				cout << player2->getName() << " wins the round" << endl;
			}
			else //Cards have equals value: start a war
			{
				bool warActive = true;
				//Each player draw 4 cards and compare the value of the last one (the 4th)

				//Array of pointers of cards that hold all the discarded cards during the war
				Card** discardedCards = new Card*[2]; //intial size is two (2 cards that initalized the war are discarded)
				int discardedCardsAmount = 2;

				//Add the cards to the discardedCards
				discardedCards[0] = new Card(*player1Card);
				discardedCards[1] = new Card(*player2Card);


				while (warActive) //As long as War is running
				{

					//If either one of the players has less than 4 cards, draw x-1 cards so the last
					//Card to be drawen will used to compare against the other
					if (player1->getTotalNumberOfCards() < 4)
					{
						for (int i = 1; i < player1->getLiveCardAmount(); i++)
						{
							this->addCardToList(*player1->pickCard(), discardedCards, discardedCardsAmount);
						}
					}
					else if (player2->getTotalNumberOfCards() < 4)
					{
						for (int i = 0; i < player2->getLiveCardAmount(); i++)
						{

							this->addCardToList(*player2->pickCard(), discardedCards, discardedCardsAmount);
						}
					}
					else
					{
						// Both player has 4 or more cards in their lists
						// Draw 3 cards
						for (int x = 0; x < 3; x++)
						{
							if (player1->hasCards())
							{
								this->addCardToList(*player1->pickCard(), discardedCards, discardedCardsAmount);
							}
							if (player2->hasCards())
							{
								this->addCardToList(*player2->pickCard(), discardedCards, discardedCardsAmount);
							}
						}
					}

					//Now draw the fourth card and compare them
					Card* playerOnesCard = NULL;
					if (player1->hasCards())
					{
						playerOnesCard = new Card(*this->player1->pickCard());
					}
					else
					{
						//If player1 has no cards to draw, play and compare with the last card the player drew
						playerOnesCard = player1Card;
					}

					Card* playerTwosCard = NULL;
					if (player2->hasCards())
					{
						playerTwosCard = new Card(*this->player2->pickCard());
					}
					else
					{
						//If player2 has no cards to draw, play and compare with the last card the player drew
						playerTwosCard = player2Card;
					}


					//Print the fourth card to the console
					cout << player1->getName() << "'s card: " << playerOnesCard->getNumber() << ", ";
					cout << player2->getName() << "'s card: " << playerTwosCard->getNumber() << "\n";

					//Player one card's has higher value
					if (playerOnesCard->getNumber() > playerTwosCard->getNumber())
					{
						//Player 1 won the war

						player1->addDeadCard(*playerOnesCard);
						player1->addDeadCard(*playerTwosCard);
						warActive = false; //War ended

						//Add all the picked cards to the winner's dead cards as well
						for (int i = 0; i < discardedCardsAmount; i++)
						{
							player1->addDeadCard(*discardedCards[i]);
						}

						//Print the winner to console
						cout << player1->getName() << " wins the round" << endl;
					}
					else if (playerOnesCard->getNumber() < playerTwosCard->getNumber())
					{
						//Player 2 won the war

						player2->addDeadCard(*playerOnesCard);
						player2->addDeadCard(*playerTwosCard);
						warActive = false; //War ended

						//Add all the picked cards to the winner's dead cards as well
						for (int i = 0; i < discardedCardsAmount; i++)
						{
							player2->addDeadCard(*discardedCards[i]);
						}

						//Print the winner to console
						cout << player2->getName() << " wins the round" << endl;


					}
					else //No winner, continue the war
					{
						warActive = true;
						addCardToList(*playerOnesCard, discardedCards, discardedCardsAmount);
						addCardToList(*playerTwosCard, discardedCards, discardedCardsAmount);

						if (discardedCardsAmount == 52)
						{
							//If discardedCards hold ALL of the available cards,
							//i.e both players don't have any more cards
							//Redistribute cards among the players again

							for (int c = 0; c < 52; c++)
							{
								//Generate a random number:
								srand(time(NULL));
								int randomNumber = rand() % (discardedCardsAmount);
								if (c % 2 == 0)
								{
									player1->addLiveCard(*discardedCards[randomNumber]);
								}
								else
								{
									player2->addLiveCard(*discardedCards[randomNumber]);
								}

								discardedCardsAmount--;
							}
						}

					}

					//Free memory allocated from all the discarded cards
					delete playerOnesCard;
					delete playerTwosCard;

				}

				delete discardedCards;

			} //End of War's scope


			delete player1Card;
			delete player2Card;
		}
		else if (player1->hasCards() && !player2->hasCards())
		{
			//Player one has cards, player two doesn't 
			//Player one wins

			cout << player1->getName() << " is the winner!!!" << endl;
			cout << "************************************************\n";
			return player1; //Break the for loop for the rounds 
		}
		else if (!player1->hasCards() && player2->hasCards())
		{
			//Player two has cards, player to doesn't
			//Player two wins
			cout << player2->getName() << " is the winner!!!" << endl;
			cout << "************************************************\n";
			return player2; // Break the for loop for the rounds 
		}
		cout << player1->getName() << " cards value: " << player1->getCardsValue() << ", ";
		cout << player2->getName() << " cards value: " << player2->getCardsValue() << "\n" << endl;


	} //End of 'Rounds' for loop

	cout << "************************************************\n";
	cout << "Game Over!!!\n";
	// If Rounds Ended, calculate the winner
	if (player1->getCardsValue() > player2->getCardsValue())
	{
		cout << player1->getName() << " is the winner!!!" << endl;
		cout << "************************************************\n";
		return player1;
	}
	else if (player1->getCardsValue() < player2->getCardsValue())
	{
		cout << player2->getName() << " is the winner!!!" << endl;
		cout << "************************************************\n";
		return player2;
	}
	else
	{
		cout << "Draw !" << endl;
		cout << "************************************************\n";
		return NULL;
	}
}

bool WarGame::addCardToList(Card &card, Card** &cardsList, int &amount)
{
	bool success = false;
	if (amount == 0)
	{
		//This is a new card to an empty list
		cardsList = new Card*[1];
		cardsList[0] = new Card(card);

		amount = 1;
		//testFoo(this->liveCards, amount);
		success = true;
	}
	else
	{
		//List is not empty

		//Copy holder array @ size of current + 1
		Card** copyHolder = new Card*[amount + 1];

		//Copy all liveCards content to to it
		for (int i = 0; i < amount; i++)
		{
			copyHolder[i] = new Card(*cardsList[i]);
		}

		//Go to last index of copyHolder and add the last card (Card &card)
		copyHolder[amount] = new Card(card);

		//Now delete the current liveCards
		//delete[] cardsList;


		//Re-declare liveCards with new values (its size increased by 1)
		cardsList = new Card*[amount + 1];
		amount++; //Amount increased by one

		for (int j = 0; j < amount; j++)
		{
			// Copy content from copyHolder array to liveCards
			cardsList[j] = new Card(*copyHolder[j]);
		}

		delete[] copyHolder;
		success = true;
	}
	return success;
}