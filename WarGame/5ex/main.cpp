/*MENIBELADEV,ID:307836684_PAZGOLDMACHER,ID:313167546*/

#include <iostream>
#include "WarPlayer.h"
#include "WarGame.h"

using namespace std;

int main()
{

	WarGame* warGame = new WarGame("Alice", "Bob");
	WarPlayer* winner = new WarPlayer(*warGame->startGame(20));

	delete warGame;
	delete winner;

	system("pause");
	return 0;
}
